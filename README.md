# UPO website

This is a repository for storing files required for building [UPO's website](http://upo.su.ust.hk/).

# Introduction

We are using the following technologies (in addition to HTML/CSS/JS) for this project's development

1. [Node JS](https://nodejs.org/en/)
2. [Gatsby](https://www.gatsbyjs.org/)
3. [Sass](https://sass-lang.com/)

# General direction

If you only plan to modify content of the website for new cabinet, this document plus global search and replace should be enough.

If you plan to change layout, styling or add new page, learn HTML, Javascript and React. [Gatsby tutorial] is a great documentation for learning after getting basic skills on HTML and Javascript.

[gatsby tutorial]: https://www.gatsbyjs.org/tutorial/

# Development

## Set up development environment

### 1. Set up Git and Git LFS

First thing first, install [git](https://git-scm.com/). We are using it for source control.

We are using [Git LFS](https://git-lfs.github.com/) for storing images and static files
Please [set up](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html) following
the tutorial from GitLab before development.

If you forgot to set up Git LFS beforehand and cloned the repository, run `git lfs pull` to fix it.

### 2. Pull from this git repository

Depending on your system. Most system just need to run `git clone https://gitlab.com/ust-upo/upo-web.git`

### 3. Install Node JS

All build tools requires [Node JS](https://nodejs.org) to execute. Follow instruction
for your platform and install it in your machine

### 4. Install dependencies for this project

Run the following command

```bash
npm install
```

When there's change in `package.json`, you need to run this command again. Usually you need to run it
on every pull when others have changed the file.

### 5. Start development server

```bash
npm run develop
```

This will compile all assets and start a server locally. Go to `localhost:8000` from your browser. You should get a preview of the webpage.

To terminate the server, press `Ctrl-C` in the terminal.

The server will compile and reload whenever there's change in source file.

## Images and data

There is a `config.yaml` file storing metadata for cabinet and concert images. All image should exist in `src/image/concert` for concert image and `src/image/exco` for cabinet member images.

There's documentation in `config.yaml` for format of each data and how to add new images. Read that documentation instead.

Generally, you should add image to the folder and write metatdata for those images in `config.yaml`. Then rebuild and see if the change has been reflected or not.

## Compile for production environment

Production means the version of the website served on a server to the public. This build will do more optimization at the cost of longer compilation time.

To produce a production build, run the following command:

```bash
npm run build

# Start http server for the produced
npm run serve
```

This command will produce production version on directory `public`. A server could be started for final checking.

# Quick guide for sample tasks

## Changing cabinet profiles

1. Put new cabinet images into `src/image/exco/`
2. Update `config.yaml` by replacing old information with new one.
3. Run `npm run develop` to see result on develop environment

## Adding new concert picture

1. Put new concert cover image into `src/image/concert`
2. Add new concert entry at the end of `concert` key in `config.yaml`. Copy objects above as a starting point
3. Run `npm run develop` to see result on develop environment

# Deployment

1. Compile in production mode (See [the guide](#compile-for-production-environment))
2. Link to [UST VPN] even you are using UST network
3. Connect to `sftp://sftp.upo.su.hkust.edu.hk` using FTP client like [FileZilla]
   - (old domain FYR: `sftp://sftp.upo.su.ust.hk`)
4. Ask for login credential from people you know
5. Delete all contents under remote folder `public_html`
6. Upload content under local `public` folder to remote `public_html`

[ust vpn]: https://itsc.ust.hk/services/cyber-security/vpn/client-installation
[filezilla]: https://filezilla-project.org/

# Analytics

This site got analytics hooked up with google analytics. To view the result,
go to <https://analytics.google.com> with society's google account.

To be honest I have no idea how to intercept the result.
