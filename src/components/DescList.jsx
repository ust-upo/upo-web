import styled from "styled-components";

/**
 * A description list element (<dl>) which applies "dl-horizontal" class
 * from bootstrap 3
 *
 * All <dt> and <dd> elements will be styled inside of DescList
 *
 * @see https://getbootstrap.com/docs/3.4/css/#horizontal-description
 */
const DescList = styled.dl`
  padding: 0.5em;

  dt {
    float: left;
    clear: left;
    width: 100px;
    text-align: right;
    font-weight: bold;
  }
  dd {
    margin: 0 0 0 110px;
  }
`;

export default DescList;
