import { GatsbyImage } from "gatsby-plugin-image";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import { md, media, sm } from "./breakpoints";

const ImgContainer = styled.div`
  position: relative;
  width: 100%;
`;

const ImgStyl = styled(GatsbyImage)`
  filter: brightness(60%) blur(4px);
  transition: filter 0.5s ease;
  max-height: 400px;
  object-fit: cover;
  width: 100%;
  vertical-align: middle;
  border-style: none;

  ${ImgContainer}:hover & {
    filter: brightness(60%) blur(8px);
  }
`;

const ContentOverlay = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  height: 100%;
  width: 100%;
  transform: translateX(-50%) translateY(-50%);
  backdrop-filter: blur(8px);
  background: #00000030;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;

  ${media(sm)} {
    width: 70%;
  }
  ${media(md)} {
    width: 45%;
  }
`;

/**
 * Placing child nodes on top of given blurred image
 */
export default function ImgOverlay({ image, alt, children, ...rest }) {
  return (
    <ImgContainer>
      <ImgStyl image={image} alt={alt} {...rest} />
      <ContentOverlay>{children}</ContentOverlay>
    </ImgContainer>
  );
}

ImgOverlay.propTypes = {
  /**
   * Same as alt attribute for <img> component. For the background image
   */
  alt: PropTypes.string.isRequired,

  /**
   * Image props for gatsby-plugin-image component
   */
  image: PropTypes.object.isRequired,

  /**
   * Nodes that should sit on top of the image
   */
  children: PropTypes.node.isRequired,
};
