import { Container } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import { media, sm } from "./breakpoints";

const JumbotContainer = styled.div`
  background-color: ${(props) => props.color};
  padding: 2rem 1rem;
  margin-bottom: 2rem;
  border-radius: 4rem;
  text-align: center;
  border: 2px solid ${(props) => props.color};

  ${media(sm)} {
    padding: 4rem 2rem;
  }
`;

const JumbotHead = styled.h1`
  font-weight: 700;
  font-size: 3.5rem;
  line-height: 1.2;
  /* text-transform: uppercase; */
  margin-bottom: 35px;
`;

const JumbotLead = styled.p`
  font-weight: 300;
  text-align: right;
  font-size: 1.25rem;
  line-height: 1.6;
  margin-bottom: 35px;
`;

/**
 * Port of bootstrap Jumbotron (kind of)
 *
 * Note that this component does not accept any child nodes.
 */
export default function Jumbotron({ title, subtitle, color }) {
  return (
    <Container>
      <JumbotContainer color={color}>
        <JumbotHead>{title}</JumbotHead>
        {subtitle != null && <JumbotLead>{subtitle}</JumbotLead>}
      </JumbotContainer>
    </Container>
  );
}

Jumbotron.propTypes = {
  /**
   * Title of the jumbotron
   */
  title: PropTypes.string.isRequired,
  /**
   * Subtitle that should render under the title
   */
  subtitle: PropTypes.string,
  /**
   * Background color. Which should be valid CSS color
   */
  color: PropTypes.string,
};

Jumbotron.defaultProps = {
  color: "#d64760",
};
