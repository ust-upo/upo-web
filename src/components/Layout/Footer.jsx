import Container from "@material-ui/core/Container";
import React from "react";
import styled from "styled-components";

const ContainerStyl = styled(Container)`
  text-align: center;
  padding: 50px 0;
`;

/**
 * Footer for all pages
 *
 * AKA boring text that no one cares
 */
export default function Footer() {
  return (
    <ContainerStyl component="footer">
      Copyright &copy; University Philharmonic Orchestra, HKUSTSU 2022
    </ContainerStyl>
  );
}
