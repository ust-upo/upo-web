import { StylesProvider } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import React from "react";
import Box from "../Box";
import Footer from "./Footer";
import GlobalStyle from "./GlobalStyle";
import Headings from "./Headings";
import Navbar from "./Navbar";

/**
 * Layout component
 *
 * All pages should be wrapped by this component
 */
const Layout = ({ children, noPt, title }) => {
  return (
    <StylesProvider injectFirst>
      <GlobalStyle />
      <Headings title={title} />
      <Navbar />

      <Box pt={noPt ? 0 : 12} textAlign="center">
        {children}
      </Box>

      <Footer />
    </StylesProvider>
  );
};

Layout.propTypes = {
  /**
   * Title of the document. Default is "University Philharmonic Orchestra, HKUSTSU"
   */
  title: PropTypes.string,

  /**
   * If true, padding-top before children will not be applied.
   *
   * Default is false as navigation bar's height is not considered by other components
   */
  noPt: PropTypes.bool,

  /**
   * Content
   */
  children: PropTypes.node.isRequired,
};

Layout.defaultProps = {
  noPt: false,
};

export default Layout;
