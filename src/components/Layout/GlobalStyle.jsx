import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    position: relative;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0.2);
    color: white;
    background-color: black;
    font-family: "Open Sans", sans-serif;
    position: relative;
    scroll-behavior: smooth;
    margin: 0;
  }

  *,
  ::after,
  ::before {
    box-sizing: border-box;
  }

  a {
    text-decoration: none;
    transition: color 0.3s ease-in-out;
    color: #42dca3;

    :hover {
      color: #1d9b6c;
      text-decoration: none;
    }
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0 0 35px;
    text-transform: uppercase;
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: 700;
    letter-spacing: 1px;
    line-height: 1.2;
  }

  p {
    margin: 0 0 25px;
    font-size: 18px;
    line-height: 1.5;
  }

  @media (min-width: 960px) {
    p {
      margin: 0 0 35px;
      font-size: 20px;
      line-height: 1.6;
    }
  }
`;

export default GlobalStyle;
