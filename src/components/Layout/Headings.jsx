import React from "react";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";

/**
 * Stuff that should exist in <head> component
 */
function Headings({ title }) {
  return (
    <Helmet>
      <html lang="en" />
      <title>{title}</title>

      <meta charset="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
    </Helmet>
  );
}

Headings.defaultProps = {
  title: "University Philharmonic Orchestra, HKUSTSU",
};

Headings.propTypes = {
  /**
   * Content for <title> component
   */
  title: PropTypes.string.isRequired,
};

export default Headings;
