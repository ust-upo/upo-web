import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import { md, media } from "../../breakpoints";

const LinkStyl = styled(({ active, ...rest }) => <Link {...rest} />)`
  color: #f8f9fa;
  text-transform: uppercase;
  padding: 15px 0;
  transition: background-color 0.3s ease-in-out;

  :hover,
  :focus {
    color: #cbd3da;
  }

  ${media(md)} {
    padding: 15px;
    background-color: ${(props) => props.active && "rgba(255, 255, 255, 0.3)"};
  }
`;

/**
 * Link in navigation bar
 */
export default function NavLink({ item, active }) {
  const anchor = item.anchor == null ? "" : `#${item.anchor}`;
  const to = item.path + anchor;

  return (
    <LinkStyl to={to} active={active}>
      {item.title}
    </LinkStyl>
  );
}

NavLink.propTypes = {
  /**
   * Item that this link points to
   * @type {MenuItem}
   */
  item: PropTypes.shape({
    path: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    anchor: PropTypes.string,
  }).isRequired,
  /**
   * If true, applies active style to this link
   */
  active: PropTypes.bool,
};

NavLink.defaultProps = {
  active: false,
};
