import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import styled from "styled-components";
import Box from "../../Box";

const LinkStyl = styled(Link)`
  color: #f8f9fa;
  display: flex;
  padding-top: 0.3125rem;
  padding-bottom: 0.3125rem;
  margin-right: 1rem;
  font-size: 1.25rem;
  line-height: inherit;
  white-space: nowrap;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  background-color: transparent;

  :hover,
  :focus,
  div:hover,
  div:focus {
    color: #cbd3da;
  }
`;

/**
 * Logo and text for navigation bar
 */
export default function NavBrand() {
  return (
    <LinkStyl to="/#">
      <StaticImage
        src="../../../image/logo.png"
        alt="UPO logo"
        height={24}
        layout="fixed"
      />
      <Box fontWeight={300}>UST</Box>
      <Box fontWeight={700} letterSpacing={5} ml={2}>
        UPO
      </Box>
    </LinkStyl>
  );
}
