import Container from "@material-ui/core/Container";
import React from "react";
import styled from "styled-components";
import useScrollY from "../../../hooks/useScrollY";
import { md, media } from "../../breakpoints";
import NavBrand from "./NavBrand";
import NavItems from "./NavItems";

/**
 * <nav> container for navigation bar
 *
 * @param props.collapse - If true, the bar should be collapsed as it passed scrolling
 */
const NavContainer = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;

  font-family: "Montserrat", sans-serif;
  background: black;
  z-index: 40;

  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  border-bottom: 1px solid rgba(255, 255, 255, 0.3);

  padding: 0.5rem 1rem;

  ${media(md)} {
    transition: background 0.5s ease-in-out, padding 0.5s ease-in-out;
    padding: ${(props) => (props.collapse ? "0" : "20px 0")};
    background: ${(props) => (props.collapse ? "black" : "transparent")};
    border-bottom: ${(props) =>
      props.collapse ? "1px solid rgba(255, 255, 255, 0.3)" : "none"};
  }
`;

const ContainerStyl = styled(Container)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
`;

/**
 * Navigation bar
 */
export default function Navbar() {
  const scrollY = useScrollY();
  const collapse = scrollY > 50;

  return (
    <NavContainer collapse={collapse}>
      <ContainerStyl>
        <NavBrand />
        {/* <NavItems collapsed={collapse} /> */}
        <NavItems />
      </ContainerStyl>
    </NavContainer>
  );
}
