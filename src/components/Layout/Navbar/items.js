/**
 * Schema for an item that should be shown in navigation bar
 * @typedef {Object} MenuItem
 * @property {string} title Text to be rendered in navigation bar
 * @property {string} path Path that the item points to
 * @property {string} anchor AKA the # part, without # prefix
 */

/**
 * Items that should exist in navigation bar
 *
 * @type {MenuItem[]}
 */
const menuItems = [
  { title: "News", path: "/", anchor: "news" },
  { title: "Join US", path: "/", anchor: "join" },
  { title: "About", path: "/", anchor: "about" },
  { title: "Concerts", path: "/concerts", anchor: "" },
  { title: "Cabinet", path: "/cabinet", anchor: "" },
  { title: "Contact", path: "/", anchor: "contact" },
  { title: "Links", path: "/", anchor: "links" },
];

export default menuItems;
