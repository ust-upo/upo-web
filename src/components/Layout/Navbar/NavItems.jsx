import {
  faFacebookSquare,
  faInstagramSquare,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Collapse from "@material-ui/core/Collapse";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useLocation } from "@reach/router";
import { OutboundLink } from "gatsby-plugin-google-analytics";
import PropTypes from "prop-types";
import React, { useState } from "react";
import styled from "styled-components";
import useActiveAnchors from "../../../hooks/useActiveAnchors";
import { md, media } from "../../breakpoints";
import menuItems from "./items";
import NavLink from "./NavLink";

const LgContainer = styled.div`
  display: flex;
  align-items: center;
`;

const NavButton = styled.button`
  margin: 0;
  overflow: visible;
  text-transform: none;

  padding: 0.25rem 0.75rem;
  font-size: 1.25rem;
  line-height: 1;
  background-color: transparent;
  border: 1px solid transparent;
  border-radius: 0.25rem;

  color: #f8f9fa;
`;

const CollapseStyl = styled(Collapse)`
  width: 100%;
`;

const MobileContainer = styled.div`
  margin: 8px 0;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

/**
 * Navigation items in navigation bar
 * @param collapsed If the parent navigation bar is collapsed,
 *   this should set to true to enable item highlighting
 */
// export default function NavItems({ collapsed }) {
export default function NavItems() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const setClosed = () => setIsOpen(false);

  const collapseNav = useMediaQuery(media(md));

  const location = useLocation();
  const activeAnchors = useActiveAnchors();
  const anchors = new Set(
    menuItems.filter(
      (item) =>
        item.path === location.pathname && activeAnchors.includes(item.anchor)
    )
  );

  if (collapseNav) {
    if (isOpen) {
      setClosed();
    }

    return (
      <LgContainer>
        {menuItems.map((item) => (
          <NavLink
            key={item.title}
            item={item}
            // active={collapsed && anchors.has(item)}
          >
            {item.title}
          </NavLink>
        ))}

        <OutboundLink
          href="mailto:su_upo@connect.ust.hk"
          target="_blank"
          rel="noopener"
          style={{ paddingLeft: "2em" }}
        >
          <FontAwesomeIcon icon={faEnvelope} style={{ color: "white" }} />
        </OutboundLink>
        <OutboundLink
          href="https://www.facebook.com/HkustsuUniversityPhilharmonicOrchestra?ref=profile"
          target="_blank"
          rel="noopener"
          style={{ paddingLeft: "1em" }}
        >
          <FontAwesomeIcon icon={faFacebookSquare} style={{ color: "white" }} />
        </OutboundLink>
        <OutboundLink
          href="https://www.instagram.com/upo_hkustsu/"
          target="_blank"
          rel="noopener"
          style={{ paddingLeft: "1em" }}
        >
          <FontAwesomeIcon
            icon={faInstagramSquare}
            style={{ color: "white" }}
          />
        </OutboundLink>
      </LgContainer>
    );
  }

  return (
    <>
      <NavButton variant="outlined" onClick={toggle}>
        <FontAwesomeIcon icon={faBars} fixedWidth />
      </NavButton>
      <CollapseStyl in={isOpen}>
        <MobileContainer>
          {menuItems.map((item) => (
            <NavLink
              key={item.title}
              item={item}
              active={anchors.has(item)}
              onClick={setClosed}
            >
              {item.title}
            </NavLink>
          ))}
        </MobileContainer>
      </CollapseStyl>
    </>
  );
}

NavItems.propTypes = {
  collapsed: PropTypes.bool,
};

NavItems.defaultProps = {
  collapsed: false,
};
