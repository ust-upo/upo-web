import styled from "styled-components";
import {
  typography,
  spacing,
  sizing,
  flexbox,
  display,
} from "@material-ui/system";

const Box = styled.div`
  ${typography}
  ${spacing}
  ${sizing}
  ${flexbox}
  ${display}
`;

/**
 * Box component from @material-ui/system
 *
 * @doc https://material-ui.com/system/basics/
 */
export default Box;
