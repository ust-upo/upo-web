import Container from "@material-ui/core/Container";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";

const SectionTitle = styled.h2`
  border-bottom: 2px solid #ffffffa0;
  padding: 0 2rem;
`;

const SectionStyl = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  text-align: center;
  padding-top: 60px;

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: 700;
    line-height: 1.2;
  }

  h2 {
    font-size: 2rem;
  }

  h3 {
    font-size: 1.75rem;
  }
`;

/**
 * Layout for each section on main page
 */
export default function Section({ id, title, children }) {
  return (
    <Container className="anchor" id={id}>
      <SectionStyl>
        {title && <SectionTitle>{title}</SectionTitle>}
        {children}
      </SectionStyl>
    </Container>
  );
}

Section.propTypes = {
  /**
   * Content for the section
   */
  children: PropTypes.node.isRequired,
  /**
   * ID for the section
   *
   * This affects highlighting of navigation bar. For list of anchors, see
   * src/components/Layout/Navbar/items.js
   */
  id: PropTypes.string.isRequired,
  /**
   * Title shown at the beginning of the section. Optional in case of using ImgOverlay
   */
  title: PropTypes.string,
};
