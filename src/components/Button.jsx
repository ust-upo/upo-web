import styled from "styled-components";

/**
 * Port of bootstrap button
 *
 * To use other components other than <button>, use `as` props
 *
 * This is a lg button in bootstrap. But this website does not use
 * other variants. So we'll hard-code it
 */
const Button = styled.button`
  display: inline-block;

  text-align: center;
  vertical-align: middle;
  user-select: none;
  padding: 0.5rem 1rem;
  font-size: 1.25rem;
  line-height: 1.5;
  text-transform: uppercase;
  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: 400;
  transition: all 0.3s ease-in-out;
  border-radius: 0;
  border: 1px solid #42dca3;
  color: #42dca3;
  background-color: transparent;

  :hover,
  :focus {
    border: 1px solid #42dca3;
    outline: none;
    color: black;
    background-color: #42dca3;
  }
`;

export default Button;
