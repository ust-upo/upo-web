// import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import styled from "styled-components";
import heroImg from "../image/hero.png";
import { md, media, sm } from "./breakpoints";
// import { CircleBtn } from "./CircleBtn";

const HeroBox = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: flex-end;
  flex-direction: column;

  width: 100%;
  min-height: 300px;
  padding-bottom: 4em;
  background: url(${heroImg}) no-repeat bottom center scroll;
  color: white;
  text-align: center;
  background-size: cover;

  ${media(sm)} {
    padding-bottom: 6em;
  }
  ${media(md)} {
    min-height: 400px;
    padding-bottom: 7em;
  }

  &:after {
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 100px;
    background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 1) 100%
    );
  }
`;

// const LinkStyl = styled(Link)`
//   color: white;
// `;

const TitleStyl = styled.span`
  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
  margin-bottom: 0.7em;

  font-size: 2.2rem;

  ${media(md)} {
    font-size: 3rem;
    letter-spacing: 2px;
  }
`;

const SubtitleStyl = styled.span`
  color: #ffffffc0;
  font-size: 0.8rem;

  ${media(md)} {
    font-size: 1.4rem;
  }

  &:before,
  &:after {
    content: "";
    display: inline-block;
    width: 5em;
    margin-left: 1em;
    margin-right: 1em;
    border-bottom: 2px solid #ffffff80;
    vertical-align: middle;

    ${media(md)} {
      width: 12em;
    }
  }
`;

/**
 * Banner section for the home page
 */
export default function HeroBanner() {
  return (
    <HeroBox>
      <TitleStyl>University Philharmonic Orchestra</TitleStyl>
      <SubtitleStyl>&nbsp;HKUSTSU&nbsp;</SubtitleStyl>

      {/* <CircleBtn>
        <LinkStyl to="/#news">
          <FontAwesomeIcon icon={faChevronDown} fixedWidth />
        </LinkStyl>
      </CircleBtn> */}
    </HeroBox>
  );
}
