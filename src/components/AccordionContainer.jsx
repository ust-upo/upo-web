import { faMusic } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import React from "react";

export default function AccordionContainer(props) {
  const accordionStyles = makeStyles((theme) => ({
    container: {
      width: "80%",
      marginBottom: "20px",
      backgroundColor: "#16212b88",
      border: "1px solid #38495a87",
      borderRadius: "5px",
    },
    header: {
      width: "60%",
      fontSize: theme.typography.pxToRem(25),
      fontWeight: 600,
      textAlign: "left",

      [theme.breakpoints.down("sm")]: {
        width: "100%",
        fontSize: theme.typography.pxToRem(16),
      },
    },
    subheader: {
      position: "absolute",
      right: "6%",
      paddingTop: theme.typography.pxToRem(8),
      color: theme.palette.text.secondary,
      fontWeight: 600,

      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    details: {
      display: "block",
      paddingLeft: "40px",
      fontSize: theme.typography.pxToRem(16),
      fontFamily: "Roboto, Helvetica, Arial, sans-serif",
      lineHeight: 1.5,
      textAlign: "left",
    },
  }));
  const AccordionClasses = accordionStyles();

  return (
    <Accordion className={AccordionClasses.container}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1bh-content"
        id="panel1bh-header"
      >
        <Typography className={AccordionClasses.header}>
          <FontAwesomeIcon icon={props.icon} fixedWidth />
          &nbsp;&nbsp; {props.header}
        </Typography>
        <Typography className={AccordionClasses.subheader}>
          {props.subheader}
        </Typography>
      </AccordionSummary>

      <AccordionDetails className={AccordionClasses.details}>
        {props.children}
        {/* <Typography>{props.children}</Typography> */}
      </AccordionDetails>
    </Accordion>
  );
}

AccordionContainer.defaultProps = {
  header: "Default header",
  subheader: "Default subheader",
  icon: faMusic,
};

AccordionContainer.propTypes = {
  header: PropTypes.string,
  subheader: PropTypes.string,
  icon: PropTypes.any,
  children: PropTypes.node,
};
