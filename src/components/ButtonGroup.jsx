import styled from "styled-components";

/**
 * A group of centered buttons that has some spacing.
 */
const ButtonGroup = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;

  & > * {
    margin: 8px;
  }
`;

export default ButtonGroup;
