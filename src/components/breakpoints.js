/**
 * Breakpoints configurations (aka responsive)
 *
 * @module
 */

export const xs = 0;
export const sm = 600;
export const md = 960;
export const lg = 1280;
export const xl = 1920;

/**
 * Create media query for given breakpoint
 *
 * @param {Number} breakpoint - Breakpoint at px
 * @returns {String} `@media` rule for CSS
 */
export function media(breakpoint) {
  return `@media (min-width: ${breakpoint}px)`;
}
