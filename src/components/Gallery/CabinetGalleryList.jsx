import Grid from "@material-ui/core/Grid";
import { trackCustomEvent } from "gatsby-plugin-google-analytics";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import PropTypes from "prop-types";
import React, { useState } from "react";
import Carousel, { Modal, ModalGateway } from "react-images";
import styled from "styled-components";
import ImageView from "./ImageView";

const Tile = styled.div`
  position: relative;
  margin: 8px;
  cursor: pointer;
`;

const ImgStyl = styled(GatsbyImage)`
  position: relative;
  border-radius: 4px;
  border: 0;
  display: block;
  overflow: hidden;
  transition: transform 0.5s ease, opactity 0.5s ease;

  :before,
  :after {
    content: "";
    pointer-events: none;
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  :before {
    opacity: 0.8;
    z-index: 1;
    background-color: ${(props) => props.color};
    transition: background-color 0.5s ease, opacity 0.5s ease;
  }

  :after {
    background-position: center;
    background-repeat: no-repeat;
    background-size: 100% 100%;
    opacity: 0.25;
    z-index: 2;
    transition: opacity 0.5s ease;
  }

  img {
    border-radius: 4px;
    display: block;
  }

  @media (any-hover: hover) {
    ${Tile}:hover & {
      transform: scale(1.1);
    }
    ${Tile}:hover &:before {
      background-color: #333333;
      opacity: 0.35;
    }
    ${Tile}:hover &:after {
      opacity: 0;
    }
  }
`;

const Description = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: background-color 0.5s ease, transform 0.5s ease;
  padding: 1em;
  border-radius: 4px;
  border-bottom: 0;
  background: #33333360;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  z-index: 3;

  position: absolute;
  top: 80%;
  left: 50%;
  width: 90%;
  transform: translate(-50%, -50%);

  h2 {
    margin: 0;
  }
`;

const Position = styled.div`
  transition: max-height 0.5s ease, opacity 0.5s ease;
  width: 100%;
  max-height: 0;
  line-height: 1.5;
  margin-top: 0.35em;
  opacity: 0;

  @media (any-hover: hover) {
    ${Tile}:hover & {
      max-height: 15em;
      opacity: 1;
    }
  }
`;

const ImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  :before {
    content: "";
    pointer-events: none;
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;

    background-color: ${(props) =>
      props.isFullscreen ? "black" : props.currentView.color};
    opacity: 0.8;
    transition: background 0.5s ease;
  }
`;

/**
 * Gallery list for cabinet page
 */
export default function CabinetGalleryList({ items, nodes }) {
  const [lightboxOpen, setLightboxOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);

  const open = (index) => {
    setLightboxOpen(true);
    setSelectedIndex(index);
    trackCustomEvent({
      category: "Gallery",
      action: "open",
      label: "cabinet",
      value: items[index]?.name,
    });
  };

  const nodeMap = new Map(nodes.map((node) => [node.base, getImage(node)]));

  if (process.env.NODE_ENV !== "production") {
    items.forEach((item) => {
      if (nodeMap.get(item.image) == null) {
        console.warn(
          `[config.yaml/cabinet]: Referring to image that does not exist in "src/image/concert". Name: "${item.name}", image: "${item.image}"`
        );
      }
    });
  }

  const renderItems = items.filter((item) => nodeMap.get(item.image) != null);

  const views = renderItems.map((item) => {
    const img = nodeMap.get(item.image);
    return {
      caption: `${item.fullName} ITSC: ${item.itsc} Department: ${item.dept}`,
      image: img,
      color: item.color,
    };
  });

  return (
    <>
      <Grid container spacing={2}>
        {renderItems.map((item, index) => (
          <Grid key={item.image} item xs={12} md={6} lg={4}>
            <Tile onClick={() => open(index)}>
              <ImgStyl
                image={nodeMap.get(item.image)}
                alt={`${item.name} as ${item.position} in UPO`}
                color={item.color}
              ></ImgStyl>
              <Description>
                <h2 style={{ fontSize: "1.1rem" }}>{item.name}</h2>
                <Position>{item.position}</Position>
              </Description>
            </Tile>
          </Grid>
        ))}
      </Grid>

      <ModalGateway>
        {lightboxOpen && (
          <Modal onClose={() => setLightboxOpen(false)}>
            <Carousel
              components={{ View: ImageView, Container: ImageContainer }}
              currentIndex={selectedIndex}
              views={views}
            />
          </Modal>
        )}
      </ModalGateway>
    </>
  );
}

CabinetGalleryList.propTypes = {
  /**
   * Array of items provided in config.
   *
   * Ordering of image list will follow this array
   */
  items: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
      /**
       * Name of the image file, including extension
       */
      image: PropTypes.string.isRequired,
      itsc: PropTypes.string.isRequired,
      dept: PropTypes.string.isRequired,
      position: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,

  /**
   * Nodes queried from graphQL API
   *
   * This should include name of file and childImageSharp for optimized image
   */
  nodes: PropTypes.arrayOf(
    PropTypes.shape({
      base: PropTypes.string.isRequired,
      childImageSharp: PropTypes.object.isRequired,
    })
  ).isRequired,
};
