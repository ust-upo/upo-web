import PropTypes from "prop-types";
import styled from "styled-components";
import { GatsbyImage } from "gatsby-plugin-image";

/**
 * Wrapper for using gatsby-image for rendering image in react-images
 *
 * The view object should have `fluid` and `caption` props described in PropTypes.
 *
 * @usage
 * <Carousel components={{View: ImageView}} />
 */
const ImageView = styled(GatsbyImage).attrs((props) => ({
  alt: props.currentView.caption,
  image: props.currentView.image,
}))`
  max-height: 100vh;
  height: auto;

  img {
    object-fit: contain !important;
  }
`;

ImageView.propTypes = {
  /**
   * Provided by react-images
   */
  currentView: PropTypes.shape({
    /**
     * Caption for this image (aka alt attribute in <img>)
     */
    caption: PropTypes.string.isRequired,
    /**
     * Image props for gatsby-plugin-image
     */
    image: PropTypes.object.isRequired,
  }).isRequired,
};

export default ImageView;
