import Grid from "@material-ui/core/Grid";
import { trackCustomEvent } from "gatsby-plugin-google-analytics";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import PropTypes from "prop-types";
import React, { useState } from "react";
import Carousel, { Modal, ModalGateway } from "react-images";
import styled from "styled-components";
import { lg, md, media } from "../breakpoints";
import ImageView from "./ImageView";

const Tile = styled.div`
  margin-bottom: 8px;
  overflow: hidden;
  display: flex;
  align-items: center;
  height: 600px;
  position: relative;
  cursor: pointer;
  filter: drop-shadow(0 0 0.4em #ffffffa0);

  ${media(md)} {
    height: 500px;
  }

  ${media(lg)} {
    height: 400px;
  }

  h2 {
    position: absolute;
    bottom: 0;
    left: 0;
    background: rgba(18, 21, 29, 0.85);
    width: 100%;
    padding: 1em;
    font-weight: 400;
    line-height: 1em;
    transition: padding 0.5s ease, color 0.5s ease;
    color: #f8f9fa;
  }

  :hover h2 {
    padding: 1em 2em;
    color: #ff69b4;
    border-left: 5px solid #ffffff80;
  }
`;

/**
 * Gallery list for concert page
 */
export default function ConcertGalleryList({ items, nodes }) {
  const [lightboxOpen, setLightboxOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);

  const open = (index) => {
    setLightboxOpen(true);
    setSelectedIndex(index);
    trackCustomEvent({
      category: "Gallery",
      action: "open",
      label: "concert",
      value: items[index]?.title,
    });
  };

  const nodeMap = new Map(nodes.map((node) => [node.base, getImage(node)]));

  if (process.env.NODE_ENV !== "production") {
    items.forEach((item) => {
      if (nodeMap.get(item.image) == null) {
        console.warn(
          `[config.yaml/concert]: Referring to image that does not exist in "src/image/concert". Name: "${item.title}", image: "${item.image}"`
        );
      }
    });
  }

  const renderItems = items.filter((item) => nodeMap.get(item.image) != null);

  const views = renderItems.map((item) => {
    const img = nodeMap.get(item.image);
    return {
      caption: item.title,
      image: img,
    };
  });

  return (
    <>
      <Grid container spacing={2}>
        {renderItems.map((item, index) => (
          <Grid key={item.image} item xs={12} md={6} lg={4}>
            <Tile onClick={() => open(index)}>
              <GatsbyImage
                style={{ position: "static" }}
                image={nodeMap.get(item.image)}
                alt={item.title}
              ></GatsbyImage>
              <h2>{item.title}</h2>
            </Tile>
          </Grid>
        ))}
      </Grid>

      <ModalGateway>
        {lightboxOpen && (
          <Modal onClose={() => setLightboxOpen(false)}>
            <Carousel
              components={{ View: ImageView }}
              currentIndex={selectedIndex}
              views={views}
            />
          </Modal>
        )}
      </ModalGateway>
    </>
  );
}

ConcertGalleryList.propTypes = {
  /**
   * Array of items provided in config.
   *
   * Ordering of image list will follow this array
   */
  items: PropTypes.arrayOf(
    PropTypes.shape({
      /**
       * Name of the image file, including extension
       */
      image: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })
  ).isRequired,

  /**
   * Nodes queried from graphQL API
   *
   * This should include name of file and childImageSharp for optimized image
   */
  nodes: PropTypes.arrayOf(
    PropTypes.shape({
      base: PropTypes.string.isRequired,
      childImageSharp: PropTypes.object.isRequired,
    })
  ).isRequired,
};
