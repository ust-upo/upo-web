import styled from "styled-components";

/**
 * The (only) circular button
 *
 * Somehow port from bootstrap
 */
export const CircleBtn = styled.button`
  width: 70px;
  height: 70px;
  margin-top: 15px;
  padding: 7px 16px;
  border: 2px solid white;
  border-radius: 100%;
  font-size: 40px;
  color: white;
  background: transparent;
  transition: background 0.3s ease-in-out;
  text-transform: uppercase;
  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: 400;

  display: flex;
  align-items: center;
  justify-content: center;
  user-select: none;
  line-height: 1.5;

  :hover {
    outline: none;
    color: white;
    background: rgba(255, 255, 255, 0.1);
  }

  /** Override hover link under this pointer */
  a:hover {
    color: white;
  }
`;
