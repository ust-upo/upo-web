import {
  faFacebookSquare,
  faInstagramSquare,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faPlayCircle } from "@fortawesome/free-regular-svg-icons";
import {
  faArrowRight,
  faGraduationCap,
  faStickyNote,
  faTheaterMasks,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { graphql, Link, useStaticQuery } from "gatsby";
import { OutboundLink } from "gatsby-plugin-google-analytics";
import { getImage } from "gatsby-plugin-image";
import React from "react";
import styled from "styled-components";
import AccordionContainer from "../components/AccordionContainer";
import { md, media } from "../components/breakpoints";
import Button from "../components/Button";
import ButtonGroup from "../components/ButtonGroup";
import HeroBanner from "../components/HeroBanner";
import ImgOverlay from "../components/ImgOverlay";
import Layout from "../components/Layout";
import Section from "../components/Section";

const ParagraphStyl = styled.p`
  padding: 0 0.5em;
  font-size: 15px;

  ${media(md)} {
    font-size: 18px;
  }
`;

export default function IndexPage() {
  const query = useStaticQuery(graphql`
    query {
      concertImage: file(relativePath: { eq: "orch.jpg" }) {
        childImageSharp {
          gatsbyImageData(layout: FULL_WIDTH, placeholder: DOMINANT_COLOR)
        }
      }

      cabinetImage: file(relativePath: { eq: "cab-group.jpg" }) {
        childImageSharp {
          gatsbyImageData(layout: FULL_WIDTH, placeholder: DOMINANT_COLOR)
        }
      }
    }
  `);

  const concertImage = getImage(query.concertImage);
  const cabinetImage = getImage(query.cabinetImage);

  return (
    <Layout noPt>
      <HeroBanner />

      <Section id="news" title="news">
        <AccordionContainer
          header="Mini Concert - Player Recruitment"
          subheader="Before 12 June"
          icon={faTheaterMasks}
        >
          Our brand new activity featuring solos, duets, and ensembles.
          <br />
          Pianists and non-members are also welcome!
          <br />
          <br />
          Details:
          <br />
          Date: TBC (between late September and early October)
          <br />
          Time: 7:00 - 8:00 pm (TBC)
          <br />
          Venue: CMA Lecture Theatre (LT-L)
          <br />
          <br />
          Quickly fill in the&nbsp;
          <OutboundLink
            href="https://forms.gle/ukmCsLwnKHdxoUoU9"
            target="_blank"
            rel="noopener"
          >
            registration form
          </OutboundLink>
          !
          <br />
          <br />
          Deadline for registration: <b>12th June 2022</b>
          <br />
          Please contact Summerloretta (98427158) for any questions.
        </AccordionContainer>

        <AccordionContainer
          header="2021 Concert Review"
          subheader="Ready on our YouTube Channel!"
          icon={faPlayCircle}
        >
          Don&apos;t worry if you have missed the performance back in 2021!
          <br />
          You can watch the videos on our YouTube channel!
          <br />
          <ul>
            <li>
              <OutboundLink
                href="https://youtu.be/KGeXNU7TdZ0"
                target="_blank"
                rel="noopener"
              >
                Promenade Concert
              </OutboundLink>
            </li>
            <li>
              <OutboundLink
                href="https://youtu.be/0kUfX_-RtXE"
                target="_blank"
                rel="noopener"
              >
                Freshmen Concert 2021
              </OutboundLink>
            </li>
          </ul>
        </AccordionContainer>
      </Section>

      <Section id="join" title="Join Us">
        <ParagraphStyl>
          If you love participating in orchestra, and you want to share your
          enjoyment of playing music with others, just join us!
          <br />
          <br />
          Players who have experiences in orchestras, concert bands and string
          orchestras will be more preferred.
          <br />
          <br />
          To join us, please fill in the application form.
        </ParagraphStyl>
        <p></p>

        <ButtonGroup>
          <Button
            as={OutboundLink}
            href="https://docs.google.com/forms/d/e/1FAIpQLSeJmcrx1TqK_OO6XynO0pW6rmA5uAhjNTNxdD8or8o65QcAuw/viewform"
            target="_blank"
            rel="noopener"
          >
            Application Form
          </Button>
        </ButtonGroup>
      </Section>

      <Section id="about" title="About UPO">
        <ParagraphStyl>
          UPO is a student-run orchestra under the Student Union of Hong Kong
          University of Science and Technology (HKUSTSU).
          <br />
          <br />
          The orchestra was previously known as the HKUSTSU Wind Ensemble, which
          merged with the Strings Ensemble in 2005. The orchestra consists of
          musicians from different backgrounds, including local, international,
          exchange students, postgraduate researchers, alumni and friends of
          HKUST.
          <br />
          <br />
          UPO aims to provide a platform for music-loving HKUST members to
          create and enjoy music. We also hope to nourish our musicians&apos;
          musicality and skills through playing orchestral music.
          <br />
          <br />
          In the past, we had been invited to perform in functions, such as the
          Outreach Day in HKUST, independent clubs&apos; orientation camps and
          inauguration ceremonies. Most recently, UPO has given a concert at
          Xiqu Centre jointly with the HKUSUPO and HKPUSUO, and Christmas
          Concert at the newly established Shaw Auditorium at HKUST.
        </ParagraphStyl>
      </Section>

      <Section id="concerts">
        <ImgOverlay image={concertImage} alt="UPO orchestra performing">
          <h2>Concerts</h2>
          <p>Posters of our previous concerts</p>
          <div>
            <Button as={Link} to="/concerts">
              Past concerts
            </Button>
          </div>
        </ImgOverlay>
      </Section>

      <Section id="cabinet">
        <ImgOverlay image={cabinetImage} alt="UPO orchestra performing">
          <h2>Vivace</h2>
          <p>Executive Committee of Session 2022-2023</p>
          <div>
            <Button as={Link} to="/cabinet">
              Committee Members
            </Button>
          </div>
        </ImgOverlay>
      </Section>

      <Section id="contact" title="Contact">
        <ButtonGroup>
          <Button
            as={OutboundLink}
            href="mailto:su_upo@connect.ust.hk"
            target="_blank"
            rel="noopener"
          >
            <FontAwesomeIcon icon={faEnvelope} fixedWidth />
            Email
          </Button>

          <Button
            as={OutboundLink}
            href="https://www.facebook.com/HkustsuUniversityPhilharmonicOrchestra?ref=profile"
            target="_blank"
            rel="noopener"
          >
            <FontAwesomeIcon icon={faFacebookSquare} fixedWidth />
            Facebook
          </Button>

          <Button
            as={OutboundLink}
            href="https://www.instagram.com/upo_hkustsu/"
            target="_blank"
            rel="noopener"
          >
            <FontAwesomeIcon icon={faInstagramSquare} fixedWidth />
            Instagram
          </Button>
        </ButtonGroup>

        <p>
          Mailbox #61, LG5, Student Center,
          <br /> Hong Kong University of Science and Technology,
          <br /> Clear Water Bay, Kowloon, Hong Kong
        </p>
      </Section>

      <Section id="links" title="Links">
        <ButtonGroup style={{ flexDirection: "column" }}>
          <Button
            as={OutboundLink}
            href="/constitution.pdf"
            target="_blank"
            rel="noopener"
            style={{ display: "flex", alignItems: "center" }}
          >
            <FontAwesomeIcon
              icon={faStickyNote}
              style={{ marginRight: "0.7em" }}
            />
            <span style={{ flexGrow: 1 }}>Constitution of UPO</span>
          </Button>

          <Button
            as={OutboundLink}
            href="http://su.ust.hk"
            target="_blank"
            rel="noopener"
            style={{ display: "flex", alignItems: "center" }}
          >
            <FontAwesomeIcon
              icon={faGraduationCap}
              style={{ marginRight: "0.7em" }}
            />
            <span style={{ flexGrow: 1 }}>HKUST Student Union</span>
          </Button>

          <Button
            as={OutboundLink}
            href="https://choir.su.ust.hk/"
            target="_blank"
            rel="noopener"
            style={{ display: "flex", alignItems: "center" }}
          >
            <FontAwesomeIcon
              icon={faArrowRight}
              style={{ marginRight: "0.7em" }}
            />
            <span style={{ flexGrow: 1 }}>The University Choir</span>
          </Button>

          <Button
            as={OutboundLink}
            href="https://www.instagram.com/hkustsu_bandsoc/"
            target="_blank"
            rel="noopener"
            style={{ display: "flex", alignItems: "center" }}
          >
            <FontAwesomeIcon
              icon={faArrowRight}
              style={{ marginRight: "0.7em" }}
            />
            <span style={{ flexGrow: 1 }}>Band Society</span>
          </Button>

          <Button
            as={OutboundLink}
            href="https://www.instagram.com/corch_hkustsu/"
            target="_blank"
            rel="noopener"
            style={{ display: "flex", alignItems: "center" }}
          >
            <FontAwesomeIcon
              icon={faArrowRight}
              style={{ marginRight: "0.7em" }}
            />
            <span style={{ flexGrow: 1 }}>Chinese Orchestra</span>
          </Button>
        </ButtonGroup>
      </Section>
    </Layout>
  );
}
