import React from "react";
import { Link } from "gatsby";
import Container from "@material-ui/core/Container";
import Layout from "../components/Layout";

export default function NotFoundPage() {
  return (
    <Layout title="Page not found">
      <Container>
        <h1>Page not found</h1>
        <p>Oops! The page you are looking for has been removed or relocated.</p>
        <Link to="/">Back to home page</Link>
      </Container>
    </Layout>
  );
}
