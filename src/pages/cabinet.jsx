import Container from "@material-ui/core/Container";
import { graphql } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import config from "../../config.yaml";
import { md, media } from "../components/breakpoints";
import CabinetGalleryList from "../components/Gallery/CabinetGalleryList";
import Layout from "../components/Layout";

export const query = graphql`
  query {
    imgs: allFile(filter: { relativeDirectory: { eq: "exco" } }) {
      nodes {
        base
        childImageSharp {
          gatsbyImageData(
            layout: CONSTRAINED
            width: 7680
            placeholder: DOMINANT_COLOR
          )
        }
      }
    }
  }
`;

const SectionTitleStyl = styled.span`
  display: inline-block;
  margin-top: 1em;
  margin-bottom: 2.5em;
  border-bottom: 2px solid #ffffffa0;
  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: bold;

  padding: 0 1em;
  font-size: 1.4rem;

  ${media(md)} {
    padding: 0 2em;
    font-size: 2.2rem;
  }
`;

export default function CabinetPage({ data }) {
  return (
    <Layout title="Committee Member, HKUST UPO">
      <SectionTitleStyl>Cabinet Members</SectionTitleStyl>
      <Container>
        <CabinetGalleryList nodes={data.imgs.nodes} items={config.cabinet} />
      </Container>
    </Layout>
  );
}

CabinetPage.propTypes = {
  data: PropTypes.shape({
    imgs: PropTypes.shape({
      nodes: PropTypes.arrayOf(
        PropTypes.shape({
          base: PropTypes.string.isRequired,
          childImageSharp: PropTypes.object.isRequired,
        })
      ).isRequired,
    }).isRequired,
  }).isRequired,
};
