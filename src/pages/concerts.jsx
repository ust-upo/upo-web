import Container from "@material-ui/core/Container";
import { graphql } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import config from "../../config.yaml";
import { md, media } from "../components/breakpoints";
import ConcertGalleryList from "../components/Gallery/ConcertGalleryList";
import Layout from "../components/Layout";

export const query = graphql`
  query {
    imgs: allFile(filter: { relativeDirectory: { eq: "concert" } }) {
      nodes {
        base
        childImageSharp {
          gatsbyImageData(
            layout: CONSTRAINED
            width: 1920
            placeholder: DOMINANT_COLOR
          )
        }
      }
    }
  }
`;

const SectionTitleStyl = styled.span`
  display: inline-block;
  margin-top: 1em;
  margin-bottom: 2.5em;
  border-bottom: 2px solid #ffffffa0;
  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: bold;

  padding: 0 1em;
  font-size: 1.4rem;

  ${media(md)} {
    padding: 0 2em;
    font-size: 2.2rem;
  }
`;

export default function ConcertsPage({ data }) {
  const concerts = config.concerts.slice();
  concerts.reverse();

  return (
    <Layout title="Concerts, HKUST UPO">
      <SectionTitleStyl>Past Concerts</SectionTitleStyl>
      <Container>
        <ConcertGalleryList nodes={data.imgs.nodes} items={concerts} />
      </Container>
    </Layout>
  );
}

ConcertsPage.propTypes = {
  data: PropTypes.shape({
    imgs: PropTypes.shape({
      nodes: PropTypes.arrayOf(
        PropTypes.shape({
          base: PropTypes.string.isRequired,
          childImageSharp: PropTypes.object.isRequired,
        })
      ).isRequired,
    }).isRequired,
  }).isRequired,
};
