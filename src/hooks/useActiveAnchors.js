import { useEffect, useState } from "react";

function getActiveAnchors() {
  if (document == null) {
    return [];
  }

  const res = [];

  const windowHeight = window.innerHeight;

  const anchors = Array.from(document.getElementsByClassName("anchor"))
    .filter((item) => item.getAttribute("id") != null)
    .map((item) => ({ element: item, top: item.getBoundingClientRect().top }));

  let prev = null;
  for (const anchor of anchors) {
    if (anchor.top >= 0 && anchor.top <= windowHeight) {
      if (prev != null && res.length === 0) {
        res.push(prev);
      }

      res.push(anchor);
    }

    if (anchor.top > windowHeight) {
      if (res.length === 0 && prev != null) {
        res.push(prev);
      }
      break;
    }

    prev = anchor;
  }

  if (res.length === 0 && prev != null) {
    res.push(prev);
  }

  return res.map((item) => item.element.getAttribute("id"));
}

/**
 * React hook that determines active anchors in the page
 *
 * Active anchors is defined by following rules:
 * - Anchors that is in current page view
 * - Last anchor on the top of page view (if available)
 *
 * Any component with CSS class "anchor" and have ID attribute is considered as an anchor
 *
 * This hook will select id of all components in view.
 *
 * @returns {string[]} Array of active anchor ID
 */
export default function useActiveAnchors() {
  const [activeAnchors, setActiveAnchors] = useState([]);

  useEffect(() => {
    function setActiveLink() {
      const anchors = getActiveAnchors();
      setActiveAnchors(anchors);
    }

    document.addEventListener("scroll", setActiveLink);
    document.addEventListener("resize", setActiveLink);

    return () => {
      document.removeEventListener("scroll", setActiveLink);
      document.removeEventListener("resize", setActiveLink);
    };
  });

  return activeAnchors;
}
