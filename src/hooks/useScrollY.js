import { useState, useEffect } from "react";

/**
 * Listen to document scroll event and get `window.scrollY` number
 *
 * @returns {number} window.scrollY value
 */
export default function useScrollY() {
  const [scrollY, setScrollY] = useState(0);
  const listener = () => {
    setScrollY(window.scrollY);
  };

  useEffect(() => {
    // Get scrollY for the first time
    setScrollY(window.scrollY);

    // Attach listener to scroll handler
    document.addEventListener("scroll", listener);
    return () => document.removeEventListener("scroll", listener);
  }, [listener]);

  return scrollY;
}
