module.exports = {
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: "module",
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: "detect",
    },
  },
  extends: ["eslint:recommended", "plugin:react/recommended", "prettier"],
  ignorePatterns: ["node_modules/", ".cache/", "public/"],
  rules: {
    "no-unused-vars": ["error", { ignoreRestSiblings: true }],
  },
};
